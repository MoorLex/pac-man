import './styles/app.scss'
import Core from './Core'

/**
 * Init game
 */
let game = new Core({
  width: 500,
  height: 500
});

/**
 * Game map
 */
let map = [
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
  [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1],
  [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1],
  [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
  [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1],
  [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1],
  [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
  [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1],
  [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
  [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1],
  [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
  [1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1],
  [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
  [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
  [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
];

/**
 * Draw grid
 */
game.onEvent('render', ({ctx, width}) => {
  width = (width - 2) / map[0].length;

  map.map((row, y) => {
    row.map((col, x) => {
      ctx.beginPath();
      ctx.strokeStyle = '#2a2a2a';
      ctx.lineWidth = 2;
      ctx.rect((x*width)+1, (y*width)+1, width, width);

      ctx.stroke();
      ctx.closePath();
    })
  })
});

/**
 * Draw map
 */
game.onEvent('render', ({ctx, width}) => {
  width = (width - 2) / map[0].length;

  map.map((row, y) => {
    row.map((col, x) => {
      ctx.beginPath();
      ctx.strokeStyle = '#0064ff';
      ctx.lineWidth = 2;

      /**
       * First in vertical wall
       */
      if(map[y][x] && id(x, y) === '000010010') {

        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, 0, Math.PI, true);

        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1 + (width/2));
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);

        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1 + (width/2));
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + width);
      }

      /**
       * Last in vertical wall
       */
      else if(map[y][x] && id(x, y) === '010010000') {

        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, 0, Math.PI, false);

        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + (width/2));

        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + (width/2));
      }

      /**
       * First in horizontal wall
       */
      else if(map[y][x] && id(x, y) === '000011000') {
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, Math.PI*.5, Math.PI*1.5, false);

        ctx.moveTo((x*width)+1 + (width/2), (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/4) + (y*width)+1);

        ctx.moveTo((x*width)+1 + (width/2), (width-(width/4)) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width-(width/4)) + (y*width)+1);
      }

      /**
       * Last in horizontal wall
       */
      else if(map[y][x] && id(x, y) === '000110000') {
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, Math.PI*.5, Math.PI*1.5, true);

        ctx.moveTo((x*width)+1, (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + (width/2), (width/4) + (y*width)+1);

        ctx.moveTo((x*width)+1, (width-(width/4)) + (y*width)+1);
        ctx.lineTo((x*width)+1 + (width/2), (width-(width/4)) + (y*width)+1);
      }

      else if(map[y][x] && id(x, y) === '000011011') {
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, Math.PI, Math.PI*1.5, false);
        ctx.moveTo((width/2) + (x*width)+1, (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/4) + (y*width)+1);
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1 + (width/2));
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '000110110') {
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, 0, Math.PI*1.5, true);
        ctx.moveTo((x*width)+1, (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + (width/2), (width/4) + (y*width)+1);
        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1 + (width/2));
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '011011000') {
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, Math.PI*.5, Math.PI, false);

        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + (width/2));

        ctx.moveTo((x*width)+1 + (width/2), (width-(width/4)) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width-(width/4)) + (y*width)+1);
      }

      else if(map[y][x] && id(x, y) === '110110000') {
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, 0, Math.PI*.5);

        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + (width/2));

        ctx.moveTo((x*width)+1, (width-(width/4)) + (y*width)+1);
        ctx.lineTo((x*width)+1 + (width/2), (width-(width/4)) + (y*width)+1);
      }

      else if(map[y][x] && id(x, y) === '000111111') {
        ctx.moveTo((x*width)+1, (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/4) + (y*width)+1);
      }

      else if(map[y][x] && id(x, y) === '111111000') {
        ctx.moveTo((x*width)+1, (width-(width/4)) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width-(width/4)) + (y*width)+1);
      }

      else if(map[y][x] && id(x, y) === '111111111') {}

      else if(map[y][x] && id(x, y) === '011011011') {
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '000110010') {
        ctx.arc((x*width)+1, width + (y*width)+1, width/4, 0, Math.PI*1.5, true);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, 0, Math.PI*1.5, true);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.moveTo((x*width)+1, (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + (width/2), (width/4) + (y*width)+1);
        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1 + (width/2));
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '000011010') {
        ctx.arc(width + (x*width)+1, width + (y*width)+1, width/4, Math.PI, Math.PI*1.5, false);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, Math.PI, Math.PI*1.5, false);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.moveTo((x*width)+1 + (width/2), (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/4) + (y*width)+1);
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1 + (width/2));
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '010011000') {
        ctx.arc(width + (x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, Math.PI*.5, Math.PI);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.moveTo((width/2) + (x*width)+1, (width/2) + (width/4) + (y*width)+1);
        ctx.lineTo((width/2) + (x*width)+1 + width, (width/2) + (width/4) + (y*width)+1);
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + (width/2));
      }

      else if(map[y][x] && id(x, y) === '010110000') {
        ctx.arc((x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI*2, true);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc((width/2) + (x*width)+1, (width/2) + (y*width)+1, width/4, Math.PI*.5, Math.PI*2, true);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.moveTo((x*width)+1, (width/2) + (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + (width/2), (width/2) + (width/4) + (y*width)+1);
        ctx.moveTo(width-(width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo(width-(width/4) + (x*width)+1, (y*width)+1 + (width/2));
      }

      else if(map[y][x] && id(x, y) === '110110110') {
        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '010110010') {
        ctx.arc((x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI*2, true);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc((x*width)+1, width + (y*width)+1, width/4, 0, Math.PI*1.5, true);
        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + width);
      }

      else if(id(x, y) === '001111111' || id(x, y) === '100111111') {
        ctx.moveTo((x*width)+1, (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/4) + (y*width)+1);
      }

      else if(id(x, y) === '111111100' || id(x, y) === '111111001') {
        ctx.moveTo((x*width)+1, (width/2) + (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/2) + (width/4) + (y*width)+1);
      }

      else if(map[y][x] && id(x, y) === '010110110') {
        ctx.arc((x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI*2, true);
        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '010011011') {
        ctx.arc(width + (x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI);
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '010011011') {
        ctx.arc(width + (x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI);
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '011011010') {
        ctx.arc(width + (x*width)+1, width + (y*width)+1, width/4, Math.PI, Math.PI*1.5, false);
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '110110010') {
        ctx.arc((x*width)+1, width + (y*width)+1, width/4, 0, Math.PI*1.5, true);
        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + width);
      }

      else if(map[y][x] && id(x, y) === '010011010') {
        ctx.arc(width + (x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc(width + (x*width)+1, width + (y*width)+1, width/4, Math.PI, Math.PI*1.5, false);
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);
      }

      /**
       * Connect left, right and bottom
       */
      else if(map[y][x] && id(x, y) === '000111010') {
        ctx.arc((x*width)+1, width + (y*width)+1, width/4, 0, Math.PI*1.5, true);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc(width + (x*width)+1, width + (y*width)+1, width/4, Math.PI, Math.PI*1.5, false);
        ctx.moveTo((x*width)+1, (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/4) + (y*width)+1);
      }

      /**
       * Connect left, right and top
       */
      else if(map[y][x] && id(x, y) === '010111000') {
        ctx.arc(width + (x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc((x*width)+1, (y*width)+1, width/4, Math.PI*.5, Math.PI*2, true);
        ctx.moveTo((x*width)+1, (width/2) + (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/2) + (width/4) + (y*width)+1);
      }

      /**
       * Connect top and bottom
       */
      else if(map[y][x] && id(x, y)[3] === '0' &&  id(x, y)[1] === '1' && id(x, y)[4] === '1' && id(x, y)[7] === '1') {
        ctx.moveTo((width/4) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width/4) + (x*width)+1, (y*width)+1 + width);
        ctx.moveTo((width-(width/4)) + (x*width)+1, (y*width)+1);
        ctx.lineTo((width-(width/4)) + (x*width)+1, (y*width)+1 + width);
      }

      /**
       * Connect left and right
       */
      else if(map[y][x] && id(x, y)[1] === '0' && id(x, y).substr(3, 3) === '111') {
        ctx.moveTo((x*width)+1, (width/4) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width/4) + (y*width)+1);
        ctx.moveTo((x*width)+1, (width-(width/4)) + (y*width)+1);
        ctx.lineTo((x*width)+1 + width, (width-(width/4)) + (y*width)+1);
      }

      else if(map[y][x]) {
        ctx.strokeStyle = game.color('#3a3a3a');
        ctx.rect((x*width)+1, (y*width)+1, width, width);
      }

      ctx.stroke();
      ctx.closePath();
    })
  })
});

function id(x, y) {
  let str = '';

  for (let i = 0; i < 3; i++)
    for (let j = 0; j < 3; j++)
      str += map[i+y-1] !== undefined ? map[i+y-1][x+j-1] !== undefined ? map[i+y-1][x+j-1] : 0 : 0;

  return str;
}

/**
 * Add canvas to page
 * Handle window resizing
 */
document.body.appendChild(game.canvas);
window.addEventListener('resize', () => { game.resize() })
