class Core {

  canvas = undefined;
  ctx = undefined;
  width = 0;
  height = 0;

  callbacks = {
    start: [],
    tick: [],
    resize: [],
    render: []
  };

  /**
   * Core constructor
   * @props Object
   * @props.width Number    Initial width
   * @props.height Number   Initial height
   * @props.canvas {node}   Canvas node element
   */
  constructor(props) {

    if(props['canvas']) this.canvas = props['canvas'];
    else this.canvas = document.createElement('canvas');

    this.ctx = this.canvas.getContext('2d');
    this.ctx.imageSmoothingEnabled = true;

    if(props['width']) this.width = props['width'];
    if(props['height']) this.height = props['height'];

    if(this.callbacks['start'] && this.callbacks['start'].length)
      this.callbacks['start'].map((callback) => { callback(this) });

    this.resize();
    this.loop();
  }

  /**
   * Resize canvas
   */
  resize() {
    this.canvas.width = this.width;
    this.canvas.height = this.height;

    if(this.callbacks['resize'] && this.callbacks['resize'].length)
      this.callbacks['resize'].map((callback) => { callback(this) });
  }

  /**
   * EventLoop - every AnimationFrame call tick and render functions
   */
  loop() {

    this.tick();
    this.render();

    requestAnimationFrame(() => { this.loop() });
  }

  /**
   * Tick handler
   */
  tick() {

    if(this.callbacks['tick'] && this.callbacks['tick'].length)
      this.callbacks['tick'].map((callback) => { callback(this) });
  }

  /**
   * Add custom event callback
   * @event String
   * @callback {function}
   */
  onEvent(event, callback) {
    if(!this.callbacks[event]) return;
    this.callbacks[event].push(callback);
  }

  /**
   * Clear canvas and call render callbacks
   */
  render() {
    this.ctx.clearRect(0, 0, this.width, this.height);

    if(this.callbacks['render'] && this.callbacks['render'].length)
      this.callbacks['render'].map((callback) => { callback(this) });
  }

  /**
   * [helper] - Convert HEX to RGBA
   *
   * @hex String      HEX color
   * @alpha Number    Color opacity
   */
  color(hex, alpha = 1) {

    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
      let c = hex.substring(1).split('');

      if(c.length === 3)
        c= [c[0], c[0], c[1], c[1], c[2], c[2]];

      c = '0x'+c.join('');

      return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+', '+alpha+')';
    }

    throw new Error('Bad Hex');
  }
}

export default Core
